#!python3

from bs4 import BeautifulSoup
import requests
import re
import os


def main():
    html = getHtml(redirect=False)
    htmlRedirected = getHtml()
    if not html or not htmlRedirected:
        print("Could not load page.")
        return 1
    toUrl = parseUrls(html)
    form = parseForm(htmlRedirected)
    sendClaim(clearUrl(toUrl), form);
    return 0;
    

def getHtml(url='http://google.com/', redirect=True):
    if not url:
        return ''
    
    output = requests.get(url, allow_redirects=redirect)
    return output.text

def parseForm(html=''):
    soup = BeautifulSoup(html, 'html.parser')
    inputs = soup.find_all('input')
    form = {}

    for inpt in inputs:
        name = inpt.get('name')
        if not name:
            continue
        form[name] = inpt.get('value');
    
    alterForm(form)
    
    auth = loadConfig()

    for key in form.keys():
        if form[key]:
            continue
        if key in auth:
            form[key] = auth.get(key)
        if not form[key]:
            form[key] = input('Form value for ' + key + ': ')

    return form

def parseUrls(html=''):
    soup = BeautifulSoup(html, 'html.parser')
    tag = soup.find('a', { 'href' : re.compile(r'^http://\d{1,3}(\.\d{1,4}){3}:\d+') })
    if not tag:
        print('<a> tag not found...')
        return ''
    return tag.get('href')

def clearUrl(url=''):
    return re.sub(r'fgtauth.*', '', url)

def sendClaim(url, formBody):
    if not url or not formBody:
        print('Claim is empty')
        return
    r = requests.post(url, formBody)
    if isOk(r.status_code):
        print('OK')
    else:
        print('Error:');
        print(r.status_code)
        print(r.text)

valueLine = re.compile(r'^([a-zA-Z0-1\-\.\_]+)=(.+)$')

def loadConfig():
    fp = None
    try:
        configLocation = os.path.dirname(os.path.realpath(__file__))
        fp = open(os.path.join(configLocation, 'auth.config'))
        if not fp:
            return {}
        output = {}
        for line in fp:
            results = valueLine.findall(line)
            if not results or len(results) == 0:
                continue
            match = results[0]
            output[match[0]] = match[1]
        fp.close()
        return output
    except Exception as e:
        if fp:
            fp.close()
        return {}

def isOk(number):
    if not number:
        return False;
    return number < 400

def alterForm(form={}):
    if 'login' in form:
        form.pop('login')
    form['username'] = ''
    form['password'] = ''
    return form

if __name__ == '__main__':
    exit(main())
