# Intro

Fazer login na rede da neppo

# Ferramentas necessárias:

Python 3 e pip

# Como executar?

## Baixar as dependencias

    python3 -m pip install -r requirements.txt

ou

    pip3 install -r requirements.txt

## Configurando o projeto

O arquivo `auth.config` possuem alguns parâmetros que podem ser modificados.
Se algumas propriedades não existirem, o usuário terá de informa-las na execução.

## Executando o projeto

    python3 main.py

ou 

    chmod u+x main.py # deve ser feito apenas uma vez
    ./main.py
